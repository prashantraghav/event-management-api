const mongoose = require('mongoose');
const modalName = 'Event';

const eventSchema = new mongoose.Schema({
    name: String,
    venue: String,
    details: String,
    dateTime: String,
    published: Boolean,
    published_url: String,
    attendees: [{type: mongoose.Schema.Types.ObjectId, reg: 'Attendee'}]
})

module.exports = mongoose.model(modalName, eventSchema);