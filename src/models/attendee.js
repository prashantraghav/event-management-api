const mongoose = require('mongoose');

const attendeeSchema = new mongoose.Schema({
    name: String,
    email: String,
    contact: String,
    events: [{type: mongoose.Schema.Types.ObjectId, reg: 'Event'}]

});

module.exports = mongoose.model('Attendee', attendeeSchema);