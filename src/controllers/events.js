const Event = require('../models/event');
const Helpers  = require ('../helpers/encryption');

class EventsController {
    static get(req, resp, next) {
        let condition = req.body;
        let methodToCall = 'find';

        if (Object.keys(req.params).length) {
            condition = {_id: req.params.id};
            methodToCall = 'findOne';
        }

        Event[methodToCall](condition, (err, result) => {
            if (err)
                next(err)

            resp.send(result);
        })
    }

    static post(req, resp, next) {
        const newEvent = new Event({...req.body, published: false, published_url: ''})
        newEvent.save((err, result) => {
            if (err)
                next(err);

            resp.send(result);
        })
    }

    static update(req, resp, next) {
        Event.updateOne({ _id: req.params.id }, {...req.body}, (err, result) => {
            if (err)
                next(err)

            resp.send(result);
        })
    }


    static delete(req, resp, next) {
        Event.deleteOne({_id: req.params.id}, (err, result) => {
            if (err)
                next(err)

            resp.send(result);
        })
    }

    static publish(req, resp, next) {
        Event.findOne({ _id: req.params.id }, (err, event) => {
            if (err)
                next(err)

            Event.updateOne({ _id: event._id }, { published: true, published_url:  Helpers.encryption.encode(event.id) }, (err, result) => {
                if (err)
                    next(err);

                resp.send(result);
            })
        })
    }

    static search(req, resp, next) {
        console.log('req', req.query);
        Event.find({$or: [{name: new RegExp(req.query.name, 'i')}, {venue: new RegExp(req.query.name, 'i')}]}, (err, results) => {
            if(err)
                next(err)
            resp.send(results);
        })
    }
}

module.exports = EventsController;