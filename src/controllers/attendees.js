const Attendee = require('../models/attendee');
const Event = require('../models/event');

const Helpers = require('../helpers/encryption');

class AttendeesController {
    static getEvent(req) {
        return Event.findOne({ _id: Helpers.encryption.decode(req.params.published_url) })
    }

    static get(req, resp, next) {
        AttendeesController.getEvent(req).exec((err, eventObj) => {
            if (err)
                next(err)

            Attendee.find({ _id: eventObj.attendees }, (err, results) => {
                if (err)
                    next(err);

                resp.send(results);
            })
        })
    }

    static post(req, resp, next) {
        console.log('post', req.params);

        const attendee = new Attendee({ ...req.body, events: Helpers.encryption.decode(req.params.published_url) });
        attendee.save((err, result) => {
            console.log('err', err);
            if (err)
                next(err);

            AttendeesController.getEvent(req).exec((err, eventObj) => {
                eventObj.attendees.push(attendee)
                Event.updateOne({ _id: Helpers.encryption.decode(req.params.published_url) }, { attendees: eventObj.attendees }, (err, result) => {
                    if (err)
                        next(err);

                    resp.send(result);
                })
            });
        })
    }
}

module.exports = AttendeesController;