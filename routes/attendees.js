var express = require('express');
var router = express.Router({mergeParams: true});

const AttendeesController = require('../src/controllers/attendees');

router.get('/', AttendeesController.get);
router.post('/', AttendeesController.post);

module.exports = router;
