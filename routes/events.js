var express = require('express');
var router = express.Router();
const EventsController = require('../src/controllers/events');

router.get('/', EventsController.get);
router.get('/search', EventsController.search)
router.get('/:id', EventsController.get);

router.post('/', EventsController.post);

router.put('/:id', EventsController.update);
router.put('/:id/publish', EventsController.publish);

router.delete('/:id', EventsController.delete);

module.exports = router;
