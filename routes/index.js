var express = require('express');
var router = express.Router();
var usersRouter = require('./users');
var eventsRouter = require('./events');
var attendeesRouter = require('./attendees');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.use('/users', usersRouter);
router.use('/events', eventsRouter);
router.use('/events/:published_url/attendees', attendeesRouter);




module.exports = router;
